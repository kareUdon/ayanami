# Ayanami
A personal conversing Japanese robot made from IBM's TJBot. 

![Alt Text](https://i.imgur.com/iZ56Cp1.jpg)
(Yes, she does need some cleaning up. I think I'm gonna paint her soon!)
 
# How to set up
To set up a talking TJ Bot using Watson Assistant and the Speech-To-Text and Text-to-Speech APIs, refer to 
this article [here](http://www.instructables.com/id/Build-a-Talking-Robot-With-Watson-and-Raspberry-Pi/)
Then, instead of adding the sample workspace as instructed, download the workspace.json file in this repo, which comes with
some beginner Japanese preset dialogues I made, which can be altered.
 
Intents are user inputs, which are what the user speaks to Ayanami, and dialogue is what she responds to the intents with. Be sure to add
the credentials in correctly, as shown in the areas with the 'xxx' in the .js file, as those are essential to making her listen and speak. Those include the conversation, speech-to-text, and text-to-speech credentials, which are automatically added as you create a project with those services. 
 
# Other notes
I'll be another function where she can translate from Japanese to English using NODE-RED. That's soon though, I'm 
quite weary from debugging her for hours. It was worth it though 👌👌👌
 
 
